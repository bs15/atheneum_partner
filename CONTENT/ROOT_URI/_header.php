<?php
$link = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
$link->set_charset("utf8");
$email = $_SESSION["user"];


if(mysqli_connect_error()){
   die("ERROR: UNABLE TO CONNECT: ".mysqli_connect_error());
}

 $sql = "SELECT * FROM ATHENEUM_PARTNERS WHERE EMAIL = '$email'";

 $result = mysqli_query($link,$sql);

 if($result){
      if(mysqli_num_rows($result)>0){
        $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
        $userName = $row["NAME"];
        $partnerId = $row["PARTNER_ID"];
        $name = explode(" ", $userName);
        $userFirst = $name[0];
      }
  }
 ?>
<!doctype html>
<html lang="en">
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="/CSS/DIZ.css" >
    <link rel="stylesheet" href="/CSS/style.css" >
    <link rel="stylesheet" href="/CSS/table.min.css" >
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

    <link rel="stylesheet" href="/CSS/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->

    <link rel="stylesheet" href="/CSS/dist/css/adminlte.css">

    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="/CSS/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- <link rel="stylesheet" href="/CSS/video.css" > -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.js"></script>
    <script type="text/javascript" src="/JS/vue.min.js"></script>



   <!--  <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
       -->

    <title>Atheneum Partner Portal</title>
  </head>
  <body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
      
  <nav class="main-header navbar navbar-expand navbar-white navbar-light fixed">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
          <a class="" href="/">
            <button class="btn btn-success">Home</button>
          </a>
        </li>
    </ul>

    <!------------- NOTIFICATION LOGIC ----------->
      <?php 
        $sqlNoti = "SELECT * FROM ATHENEUM_NOTIFICATION WHERE USER_ID='$partnerId'";
        $resultNoti = mysqli_query($link, $sqlNoti);
        $active = 0;
        while($row = mysqli_fetch_array($resultNoti,MYSQLI_ASSOC)){
          if($row['STATUS'] == 'PENDING'){
            $active++;
          }
        }
       ?>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        </li>
      <!-- Messages Dropdown Menu -->
      <?php if ($_SESSION['LoggedIn']): ?>
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="profile">
              <?php echo $userFirst; ?>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link" href="notification">
              <i class="far fa-bell"></i>
              <span class="badge badge-warning navbar-badge"><?php if($active !=0) echo $active; ?></span>
            </a>
          </li>
          <li class="nav-item">
            <a class="btn btn-success" href="signOut">
              Sign Out
            </a>
          </li>
        </ul>

      <?php endif; ?>
      <?php if (!$_SESSION['LoggedIn']): ?>
        <li class="nav-item">
          <a class="" href="signIn">
            <button class="btn btn-success">Sign In</button>
          </a>
        </li>
      <?php endif; ?>
      
    </ul>
  </nav>
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->

    <a href="/" class="brand-link" style="">
      <img src="/IMAGES/logo.jpeg" alt="Udaan logo" class="brand-image img-circle elevation-3"
           style="opacity: 1; background: #fff;" width="50" height="60">
      <span class="brand-text font-weight-light">Atheneum</span>
    </a>

    <!-- Sidebar -->
    <?php if ($_SESSION['LoggedIn']): ?>
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="profile" class="d-block"><i class="fas fa-user-circle text-info mr-1"></i> <?php echo $userName; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          <li class="nav-item">
            <a href="studentmanage" class="nav-link">
              <i class="nav-icon fas fa-graduation-cap"></i>
              <p>
                  Students
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="rewards" class="nav-link">
              <i class="nav-icon fas fa-trophy"></i>
              <p>
                  Rewards
              </p>
              <span class="right badge badge-danger">New</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="comingSoon" class="nav-link">
              <i class="nav-icon fas fa-university"></i>
              <p>
                  Class Scheduling
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="invoices" class="nav-link">
              <i class="nav-icon fas fa-money"></i>
              <p>
                  Invoices
              </p>
              <span class="right badge badge-danger">New</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="comingSoon" class="nav-link">
              <i class="nav-icon fas fa-envelope-open"></i>
              <p>
                  Marketing Collaterals
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <?php else: ?>
      <a href="sign" class="brand-link">
        <!-- <img src="/IMAGES/logo5.png" alt="Beanstalk logo" class="brand-image img-circle elevation-3"
             style="opacity: 1"> -->
        
        <span class="brand-text font-weight-light" style="text-align: center;"><i class="fas fa-user-circle text-info mr-1"></i> Sign In</span>
      </a>

    <?php endif; ?>
    <!-- /.sidebar -->
  </aside>
  <div class="content-wrapper">
  <section class="content">
    <br>
      

   
  
   
  

