<?php 
if ($_SESSION['LoggedIn']) {
  echo "<script>window.location.href = '/'</script>";
}

 ?>
<!DOCTYPE html>
<html>
<head>
  <title>Atheneum Partner Login</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
  </script>
  <link rel="stylesheet" type="text/css" href="/CSS/login.css">
</head>
<body>
  <div class="container">
    <?php
      if ($GLOBALS['alert_info']!="") {
        echo $GLOBALS['alert_info'];
      }
    ?>
    <div class="wrapper fadeInDown">
      <div id="formContent">
        <!-- Tabs Titles -->
        <h2 class="active"> Sign In </h2>
        <!-- Icon -->
        <div class="fadeIn first">
          <img src="/IMAGES/logo.jpeg" width="100" height=""  alt="Atheneum Icon" />
        </div>

        <!-- Login Form -->
        <form method="POST">
          <input type="hidden" name="s_Hash" value="<?php echo $_SESSION['s_Hash']; ?>">
          <input type="hidden" name="formName" value="partnerSignIn">
          <input type="email" id="login" class="fadeIn second" name="email" placeholder="Email" required>
          <input type="password" id="password" class="fadeIn third" name="password" placeholder="password" required>
          <input type="submit" class="fadeIn fourth" value="Log In">
        </form>

        <!-- Remind Passowrd -->
        <div id="formFooter">
          <a class="underlineHover" href="forgotPassword">Forgot Password?</a>
        </div>

      </div>
    </div>

  </div>
</body>
</html>
