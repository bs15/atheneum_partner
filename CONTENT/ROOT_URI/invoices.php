<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>
<style type="text/css">
  .getRewards{
    background: #616161;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #9bc5c3, #616161);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #9bc5c3, #616161); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

  }
</style>


<?php if($_SESSION['LoggedIn']): ?>
  <?php 
    $sqlFetch = "SELECT * FROM ATHENEUM_PARTNERS WHERE PARTNER_ID = '$partnerId'";
    $resultFetch = mysqli_query($link, $sqlFetch);
    $rowFetch = mysqli_fetch_array($resultFetch,MYSQLI_ASSOC);
    $walletMoney = $rowFetch['WALLET_MONEY'];
    $bankDetails = $rowFetch['BANK_DETAILS'];
    $partnerName = $rowFetch['NAME'];
    $partnerEmail = $rowFetch['EMAIL'];
    $partnerPhone = $rowFetch['PHONE'];
    $bankDetails = $rowFetch['BANK_DETAILS'];
    
    
   ?>

<div class="container col-md-8 col-sm-12 mx-auto" id="vapp">
  <h2 class="text-center">Invoice Management</h2>
	<div class="">
    <?php 
      // INVOICE DETAILS

      $sqlInvoice = "SELECT * FROM ATHENEUM_INVOICES WHERE USER_ID = '$partnerId' ORDER BY STATUS";
      $resultInvoice = mysqli_query($link, $sqlInvoice);
      if (mysqli_num_rows($resultInvoice) == 0) {
        echo '<div class="alert alert-warning">No Invoices raised till now<div>';
      }
      while ($rowInvoice = mysqli_fetch_array($resultInvoice,MYSQLI_ASSOC)) { ?>
        <?php if ($rowInvoice['STATUS'] == 'RAISED') { ?>
          <div class="card shadow bg-warning">
            <div class="card-header">
              <span class="float-left">Invoice Id:- # <?php echo $rowInvoice['INVOICE_ID']; ?> </span>
              <span class="float-right">RAISED </span>
            </div>
            <div class="card-body">
              <h4>Partner Id:- <?php echo $partnerId; ?></h4>
              <h4>Name:- <?php echo $partnerName; ?></h4>
              <h4>Amount:- <?php echo $rowInvoice['CURRENCY']." ".$rowInvoice['AMOUNT']; ?></h4>
              <h4>Date Raised:- <?php echo $rowInvoice['DATE_RAISED']; ?></h4>
            </div>
          </div>
        <?php }else{ ?>
          <div class="card shadow bg-success">
            <div class="card-header">
              <span class="float-left">Invoice Id:- # <?php echo $rowInvoice['INVOICE_ID']; ?> </span>
              <span class="float-right">PROCESSED </span>
            </div>
            <div class="card-body">
              <h4>Partner Id:- <?php echo $partnerId; ?></h4>
              <h4>Name:- <?php echo $partnerName; ?></h4>
              <h4>Amount:- <?php echo $rowInvoice['CURRENCY']." ".$rowInvoice['AMOUNT']; ?></h4>
              <h4>Date Raised:- <?php echo $rowInvoice['DATE_RAISED']; ?></h4>
              <h4>Date Processed:- <?php echo $rowInvoice['DATE_PROCESSED']; ?></h4>
              <h4>Processed By:- Atheneum Global College</h4>
            </div>
          </div>
        <?php }
       }
      


     ?>
  </div>
	 
</div>
<?php else: ?>
  <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12 ml-auto mr-auto">
      <div class="alert">You are not allowed to access the page. Please <a href="signIn">Sign in</a> to see the page.</div>
    </div>
  </div>

<?php endif; ?>
