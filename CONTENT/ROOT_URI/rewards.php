<style type="text/css">
  .getRewards{
    background: #616161;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #9bc5c3, #616161);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #9bc5c3, #616161); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

  }
</style>
<script type="text/javascript">
  function sendMail(email, subject, message) {
    let formData = new FormData();
      formData.append('sendMail', 'true');
      formData.append('reciever', email);
      formData.append('sender', 'Admin');
      formData.append('senderMail', 'no-reply@atheneumglobal.com');
      formData.append('subject', subject);
      formData.append('message', message);
      fetch("https://mailapi.teenybeans.in/", {
          method: "POST",
          body:formData,
        }).then(
            function(response) {
            response.json().then(function(data) {
              console.log(data);
            });
          }
        )
        .catch(function(err) {
          console.log('Fetch Error :-S', err);
        });
  }
</script>


<?php if($_SESSION['LoggedIn']): ?>
  <?php 
    $sqlFetchWallet = "SELECT * FROM ATHENEUM_PARTNERS WHERE PARTNER_ID = '$partnerId'";
    $resultFetchWallet = mysqli_query($link, $sqlFetchWallet);
    $rowWallet = mysqli_fetch_array($resultFetchWallet,MYSQLI_ASSOC);
    $walletMoney = $rowWallet['WALLET_MONEY'];
    $earnedMoney = $rowWallet['EARNED_MONEY'];
    $bankDetails = $rowWallet['BANK_DETAILS'];
    $partnerName = $rowWallet['NAME'];
    $partnerEmail = $rowWallet['EMAIL'];
    
    if ($earnedMoney) {
      $earnedMoney = json_decode($earnedMoney);
      $inrEarned = $earnedMoney->inr;
      $dollarEarned = $earnedMoney->dollar;  
    }else{
      $inrEarned = 0;
      $dollarEarned = 0;
    }

    if ($walletMoney) {
      $walletMoney = json_decode($walletMoney);
      $inr = $walletMoney->inr;
      $dollar = $walletMoney->dollar;  
    }else{
      $inr = 0;
      $dollar = 0;
    }
    if ($inr >= 10000) {
      $activeInr = true;
    }
    if ($dollar >= 150) {
      $activeDollar = true;
    }
    if (isset($_GET['raiseInrInvoice'])) {
      if (!$bankDetails) {
        echo '<div class="alert alert-warning text-center">Please fill your <a href="profile">Bank details</a> first</div>';
      }else{
        if ($inr >= 10000) {
          $today = date("Y-m-d");
          $invoiceId = D_create_UserId();
          $sqlInvoice = "INSERT INTO ATHENEUM_INVOICES (`INVOICE_ID`, `USER_ID`, `CURRENCY`, `AMOUNT`, `DATE_RAISED`, `STATUS`) VALUES ('$invoiceId', '$partnerId', 'INR', '$inr', '$today', 'RAISED')";
          $resultInvoice = mysqli_query($link, $sqlInvoice);
          if ($resultInvoice) {
            //Decrease Wallet Money
            $walletMoney = array('inr' => 0, 'dollar' => $dollar);
            $walletMoney = json_encode($walletMoney);
            // UPDATE RECORD 
            $sqlUpdateRecord = "UPDATE ATHENEUM_PARTNERS SET WALLET_MONEY = '$walletMoney' WHERE PARTNER_ID = '$partnerId'";
            $resultUpdateRecord = mysqli_query($link, $sqlUpdateRecord);
            if ($resultUpdateRecord) {
              // echo '<div class="alert alert-success text-center">Successfully Raised Invoice. Please wait for admin confirmation!</div>';
              // Send Notification
              $sqlNoti = "INSERT INTO ATHENEUM_NOTIFICATION (`USER_ID`, `USER_TYPE`, `TITLE`, `MESSAGE`, `LINK`, `STATUS`) VALUES ('ADMIN', 'ADMIN', 'INVOICE RAISED', '".$partnerId." - ".$partnerName." has raised an invoice', 'atheneumpartners', 'PENDING')";
              $resultNoti = mysqli_query($link, $sqlNoti);
              //SEND MAIL TO ADMIN
              $subject = "Invoice raised by atheneum partner";
              $msg = "<html>
                          <body style='text-align:center; background:#67E6DC'>
                          <h2>Welcome to Atheneum Global Teacher Traing College </h2>
                          <h2>".$partnerName." - a registered partner of atheneumglobal.education has raised an invoice. Please <a href='https://admin.atheneumglobal.education/'>login to admin portal</a> to know in details.</h2>
                          <h3>We're in the business of breaking down barriers. Find out how we make practical, career-focused learning more accessible than ever before.</h3>
                          <h3>Thank you.</h3>
                          <h3>- Team Atheneum.</h3>
                          </body>
                        </html>";
              echo '<script>sendMail(`abhishekbanerjea@gmail.com`, `'.$subject.'`, `'.$msg.'`)</script>';

              // SEND MAIL TO PARTNER

              $subject = "Successfully Raised Invoice";
              $msg = "<html>
                          <body style='text-align:center; background:#67E6DC'>
                          <h2>Welcome ".$partnerName." to Atheneum Glocal Teacher Training College. You have successfully raised an invoice with id ".$invoiceId.".Here are the details:- </h2>
                          <table>
                            <tr>
                            <th>Invoice Id:- </th>
                            <td>".$invoiceId."</td>
                            </tr>
                            <tr>
                            <th>User name:- </th>
                            <td>".$partnerName."</td>
                            </tr>
                            <tr>
                            <th>Amount:- </th>
                            <td>INR".$inr."</td>
                            </tr>
                            <tr>
                            <th>Date of Raise:- </th>
                            <td>".$today."</td>
                            </tr>
                          </table>
                          <h2>Please <a href='https://partners.atheneumglobal.education/'>login to admin portal</a> to know in details.</h2>
                          <h3>We're in the business of breaking down barriers. Find out how we make practical, career-focused learning more accessible than ever before.</h3>
                          <h3>Thank you.</h3>
                          <h3>- Team Atheneum.</h3>
                          </body>
                        </html>";
              echo '<script>sendMail(`'.$partnerEmail.'`, `'.$subject.'`, `'.$msg.'`);</script>';

              echo '<script>alert("Successfully Raised Invoice. Please wait for admin confirmation!");</script>';
              echo '<script>window.location.href="rewards";</script>';
            }
          }
        }else{
          // echo '<div class="alert alert-warning text-center">Your wallet balance is not sufficient.</div>';
          echo '<script>alert("Your wallet balance is not sufficient.");</script>';
          echo '<script>window.location.href="rewards";</script>';
        }
      }
    }
    if (isset($_GET['raiseDollarInvoice'])) {
      if (!$bankDetails) {
        echo '<div class="alert alert-warning text-center">Please fill your <a href="profile">Bank details</a> first</div>';
      }else{
        if ($dollar >= 150) {
          $today = date("Y-m-d");
          $invoiceId = D_create_UserId();
          $sqlInvoice = "INSERT INTO ATHENEUM_INVOICES (`INVOICE_ID`, `USER_ID`, `CURRENCY`, `AMOUNT`, `DATE_RAISED`, `STATUS`) VALUES ('$invoiceId', '$partnerId', 'USD', '$dollar', '$today', 'RAISED')";
          $resultInvoice = mysqli_query($link, $sqlInvoice);
          if ($resultInvoice) {
            //Decrease Wallet Money
            $walletMoney = array('inr' => $inr, 'dollar' => 0);
            $walletMoney = json_encode($walletMoney);
            // UPDATE RECORD 
            $sqlUpdateRecord = "UPDATE ATHENEUM_PARTNERS SET WALLET_MONEY = '$walletMoney' WHERE PARTNER_ID = '$partnerId'";
            $resultUpdateRecord = mysqli_query($link, $sqlUpdateRecord);
            if ($resultUpdateRecord) {
              // echo '<div class="alert alert-success text-center">Successfully Raised Invoice. Please wait for admin confirmation!</div>';
              // Send Notification
              $sqlNoti = "INSERT INTO ATHENEUM_NOTIFICATION (`USER_ID`, `USER_TYPE`, `TITLE`, `MESSAGE`, `LINK`, `STATUS`) VALUES ('ADMIN', 'ADMIN', 'INVOICE RAISED', '".$partnerId." - ".$partnerName." has raised an invoice', 'atheneumpartners', 'PENDING')";
              $resultNoti = mysqli_query($link, $sqlNoti);

              //SEND MAIL TO ADMIN
              $subject = "Invoice raised by atheneum partner";
              $msg = "<html>
                          <body style='text-align:center; background:#67E6DC'>
                          <h2>Welcome to Atheneum Global Teacher Traing College </h2>
                          <h2>".$partnerName." - a registered partner of atheneumglobal.education has raised an invoice. Please <a href='https://admin.atheneumglobal.education/'>login to admin portal</a> to know in details.</h2>
                          <h3>We're in the business of breaking down barriers. Find out how we make practical, career-focused learning more accessible than ever before.</h3>
                          <h3>Thank you.</h3>
                          <h3>- Team Atheneum.</h3>
                          </body>
                        </html>";
              echo '<script>sendMail(`abhishekbanerjea@gmail.com`, `'.$subject.'`, `'.$msg.'`)</script>';

              // SEND MAIL TO PARTNER

              $subject = "Successfully Raised Invoice";
              $msg = "<html>
                          <body style='text-align:left; background:#67E6DC'>
                          <h2>Welcome ".$partnerName." to Atheneum Glocal Teacher Training College. You have successfully raised an invoice with id ".$invoiceId.".Here are the details:- </h2>
                          <table>
                            <tr>
                            <th>Invoice Id:- </th>
                            <td>".$invoiceId."</td>
                            </tr>
                            <tr>
                            <th>User name:- </th>
                            <td>".$partnerName."</td>
                            </tr>
                            <tr>
                            <th>Amount:- </th>
                            <td>USD".$dollar."</td>
                            </tr>
                            <tr>
                            <th>Date of Raise:- </th>
                            <td>".$today."</td>
                            </tr>
                          </table>
                          <h2>Please <a href='https://partners.atheneumglobal.education/'>login to admin portal</a> to know in details.</h2>
                          <h3>We're in the business of breaking down barriers. Find out how we make practical, career-focused learning more accessible than ever before.</h3>
                          <h3>Thank you.</h3>
                          <h3>- Team Atheneum.</h3>
                          </body>
                        </html>";
              echo '<script>sendMail(`'.$partnerEmail.'`, `'.$subject.'`, `'.$msg.'`)</script>';
              echo '<script>alert("Successfully Raised Invoice. Please wait for admin confirmation!");</script>';
              echo '<script>window.location.href="rewards";</script>';
            }
          }
        }else{
          // echo '<div class="alert alert-warning text-center">Your wallet balance is not sufficient.</div>';
          echo '<script>alert("Your wallet balance is not sufficient");</script>';
              echo '<script>window.location.href="rewards";</script>';
        }
      }
    }
    
   ?>

<div class="container col-md-8 col-sm-12 mx-auto" id="vapp">
  <h2 class="text-center">Rewards Section</h2>
  <div class="card getRewards shadow">
    <div class="card-body text-center">
      <h3 style="color: #fff; font-style: ; font-weight: bold;">Now get your rewards money in your bank account</h3>
      <h5 style="color: #fff; font-style: italic; font-weight: bold;">*Earn 10000 INR to raise an invoice*</h5>
      <h5 style="color: #fff; font-style: italic; font-weight: bold;">*Earn 150 USD to raise an invoice*</h5>
      <h6 style="color: #fff; font-style: italic; font-weight: ;">*Fill your <a href="profile" style="color: pink">Bank Details</a> to get the reward*</h6>
    </div>
  </div>
	<div class="card shadow">
    <div class="card-header">
        Wallet Balance <img src="IMAGES/wallet.png" height="20">
     </div>
     <div class="card-body">
      <div class="row">
        <div class="col-6">
          <h4 class="float-left"> INR Balance:- <img src="IMAGES/rupee.svg" height="30"> <?php echo $inr; ?> </h4>
        </div>
        <div class="col-6">
          <?php if ($activeInr) { ?>
            <a href="rewards?raiseInrInvoice" class="float-right btn btn-success">Raise Invoice</a>
         <?php }else{ ?>
            <button class="float-right btn btn-success" disabled title="10000 INR is the minimum amount to raise invoice">Raise Invoice </button>
         <?php } ?>
          
        </div>
      </div>
    <hr>
      <div class="row">
        <div class="col-6">
          <h4 class="float-left">USD Balance:- <img src="IMAGES/money.svg" height="30"> <?php echo $dollar; ?> </h4>
        </div>
        <div class="col-6">
          <?php if ($activeDollar) { ?>
            <a href="rewards?raiseDollarInvoice" class="float-right btn btn-success">Raise Invoice</a>
         <?php }else{ ?>
            <button class="float-right btn btn-success" disabled title="150 USD is the minimum amount to raise invoice">Raise Invoice </button>
         <?php } ?>
        </div>
      </div>
     </div>
  </div>
  <div class="card shadow">
    <div class="card-header">
        Earned Money <img src="IMAGES/moneyearned.svg" height="25">
     </div>
     <div class="card-body">
      <div class="row">
        <div class="col-6">
          <h4 class="float-left"> Earned INR:- <img src="IMAGES/rupee.svg" height="30"> <?php echo $inrEarned; ?> </h4>
        </div>
      </div>
    <hr>
      <div class="row">
        <div class="col-6">
          <h4 class="float-left">Earned USD:- <img src="IMAGES/money.svg" height="30"> <?php echo $dollarEarned; ?> </h4>
        </div>
      </div>
     </div>
  </div>
	 
</div>
<?php else: ?>
  <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12 ml-auto mr-auto">
      <div class="alert">You are not allowed to access the page. Please <a href="signIn">Sign in</a> to see the page.</div>
    </div>
  </div>

<?php endif; ?>
