<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>
<script type="text/javascript">

</script>

<?php 
	$link = mysqli_connect(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
  $email = $_SESSION["user"];
  $userId = $_SESSION["userId"];
  $sql = "SELECT * FROM ATHENEUM_PARTNERS WHERE PARTNER_ID = '$userId'";
  $result = mysqli_query($link,$sql);

 if($result){
    if(mysqli_num_rows($result)>0){
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $userName = $row["NAME"];
      $email = $row["EMAIL"];
      $phone = $row["PHONE"];
    }
  }
  $sqlInvoice = "SELECT * FROM ATHENEUM_INVOICES WHERE USER_ID = '$partnerId' AND STATUS = 'RAISED'";
  $resultInvoice = mysqli_query($link, $sqlInvoice);
  $raisedInvoice = mysqli_num_rows($resultInvoice);
  
  $sqlInvoice2 = "SELECT * FROM ATHENEUM_INVOICES WHERE USER_ID = '$partnerId' AND STATUS = 'PROCESSED'";
  $resultInvoice2 = mysqli_query($link, $sqlInvoice2);
  $processedInvoices = mysqli_num_rows($resultInvoice2);
  
  $sqlRegisteredStudent = "SELECT * FROM ATHENEUM_STUDENT WHERE PARTNER_ID = '$partnerId'";
  $resultRegisteredStudent = mysqli_query($link, $sqlRegisteredStudent);
  $totalStudent = mysqli_num_rows($resultRegisteredStudent);

  $sqlEnrolledStudent = "SELECT * FROM ATHENEUM_STUDENT WHERE PARTNER_ID = '$partnerId' AND PAID != 0";
  $resultEnrolledStudent = mysqli_query($link, $sqlEnrolledStudent);
  $enrolledStudent = mysqli_num_rows($resultEnrolledStudent);
 ?>


<?php if($_SESSION['LoggedIn']){ ?>

<div class="container">
  <div id="fb-root"></div>
	
	 <div class="row">
        <div class="col-md-12">
          <h1 class="display-7 text-center">Partner Dashboard</h1>
          <div class="user">
            <p class="lead text-muted text-center">Welcome <?php echo $userName; ?></p>
            <!-- <a href="changePassword" class="btn btn-danger"><i class="fa fa-lock"></i>&nbsp;Change Password</a> -->
                <!-- <a href="changePassword" class="btn btn-warning"><i class="fas fa-user"></i>&nbsp;Update Details</a> -->
          </div>
          <!-- <br> -->
          <div class="row col-md-8 col-sm-12 mx-auto">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3><?php echo $totalStudent; ?></h3>

                <p>Registered Students</p>
              </div>
              <div class="icon">
                <i class="fa fa-id-badge" aria-hidden="true"></i>
              </div>
              <a href="studentmanage" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><?php echo $enrolledStudent; ?></h3>

                <p>Enrolled Students</p>
              </div>
              <div class="icon">
                <i class="fas fa-graduation-cap"></i>
              </div>
              <a href="studentmanage" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->

           <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><?php echo $raisedInvoice; ?></h3>

                <p>Raised Invoices</p>
              </div>
              <div class="icon">
                <i class="fa fa-file-text" aria-hidden="true"></i>
              </div>
              <a href="invoices" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>


          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $processedInvoices; ?></h3>

                <p>Processed Invoices</p>
              </div>
              <div class="icon">
                <i class="fas fa-money"></i>
              </div>
              <a href="invoices" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          
        </div>

          <!-- ---------------USER DETAILS TABLE ------------- -->

          <div class="row">
          <div class="col-md-8 col-sm-12 ml-auto mr-auto">
            <div class="card shadow">
              <div class="card-header pb-1">
                <h5 class="float-left">User Details</h5>
                <a href="profile" class="float-right btn btn-primary" style="text-decoration: none;">Edit <i class="fa fa-pencil-square" aria-hidden="true"></i></a>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-borderless">
                    <tr>
                      <th>Full Name:- </th>
                      <td><?php echo $userName; ?></td>
                    </tr>
                    <tr>
                      <th>Email:- </th>
                      <td><?php echo $email; ?></td>
                    </tr>
                     <tr>
                      <th>Phone:- </th>
                      <td><?php echo $phone; ?></td>
                    </tr>
                    <tr>
                      <th>Password:- </th>
                      <td>******</td>
                    </tr>
                  </table>
                </div>
              </div> 
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
          

        </div>
      </div>
</div>
<?php }else{
  echo "<script>window.location.href = 'signIn'</script>";
} ?>
<script type="text/javascript">
   $(function(){
      $("#upload_pro").on('click', function(e){
          e.preventDefault();
          $("#upload1:hidden").trigger('click');
      });
    });
   function deleteUser(userId){
    console.log(userId);
   }
</script>
