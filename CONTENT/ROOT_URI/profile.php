<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>
<style type="text/css">
  .getRewards{
    background: #616161;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #9bc5c3, #616161);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #9bc5c3, #616161); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

  }
</style>


<?php if($_SESSION['LoggedIn']): ?>
  <?php 
    $sqlFetch = "SELECT * FROM ATHENEUM_PARTNERS WHERE PARTNER_ID = '$partnerId'";
    $resultFetch = mysqli_query($link, $sqlFetch);
    $rowFetch = mysqli_fetch_array($resultFetch,MYSQLI_ASSOC);
    $walletMoney = $rowFetch['WALLET_MONEY'];
    $bankDetails = $rowFetch['BANK_DETAILS'];
    $partnerName = $rowFetch['NAME'];
    $partnerEmail = $rowFetch['EMAIL'];
    $partnerPhone = $rowFetch['PHONE'];
    $bankDetails = $rowFetch['BANK_DETAILS'];
    if ($bankDetails) {
      $bankDetails = json_decode($bankDetails);
      $accNo = $bankDetails->accNo;
      $ifsc = $bankDetails->ifsc;
      $bankName = $bankDetails->bankName;
      $upiId = $bankDetails->upiId;
    }
    if ($walletMoney) {
      $walletMoney = json_decode($walletMoney);
      $inr = $walletMoney->inr;
      $dollar = $walletMoney->dollar;  
    }else{
      $inr = 0;
      $dollar = 0;
    }

    if (isset($_POST['submitProfile'])) {
      if (isset($_POST['partnerName'])) 
        $editPartnerName = $_POST['partnerName'];
      if (isset($_POST['partnerPhone'])) 
        $editPhone = $_POST['partnerPhone'];
      if (isset($_POST['bankAccNo'])) 
        $editAccNo = $_POST['bankAccNo'];
      if (isset($_POST['ifsc'])) 
        $editIfsc = $_POST['ifsc'];
      if (isset($_POST['bankName'])) 
        $editBankName = $_POST['bankName'];
      if (isset($_POST['upiId'])) 
        $editUpiId = $_POST['upiId'];
      if (($editAccNo && $editBankName && $editIfsc) || $editUpiId) {
        $bankDetails = array('accNo' => $editAccNo, 'ifsc' => $editIfsc, 'bankName' => $editBankName, 'upiId' => $editUpiId);
        $bankDetails = json_encode($bankDetails);
        $sqlUpdate = "UPDATE ATHENEUM_PARTNERS SET `NAME` = '$editPartnerName', `PHONE` = '$editPhone', `NAME` = '$editPartnerName', `BANK_DETAILS` = '$bankDetails' WHERE `PARTNER_ID` = '$partnerId'";
        $resultUpdate = mysqli_query($link, $sqlUpdate);
        if ($resultUpdate) {
          echo '<script>alert("Successfully Updated profile");</script>';
          echo '<script>window.location.reload();</script>';
        }else{
          echo mysqli_error($link);
        }
      }else{
        echo "<div class='alert alert-warning'>Fill Up Bank Details!</div>";
      }
    }
    
   ?>

<div class="container col-md-8 col-sm-12 mx-auto" id="vapp">
  <h2 class="text-center">Partner Profile</h2>
	<div class="card shadow">
    <form method="POST">
      <div class="card-header">
          Personal Details
       </div>
       <div class="card-body text-center">
        <a href="changePassword" class="btn btn-danger"><i class="fa fa-lock"></i>&nbsp;Change Password</a>
        <hr>
          <div class="row text-left">
            <div class="col-6">
              <div class="form-group">
                <label>Id</label>
                <input type="text" class="form-control" readonly placeholder="Id" value="<?php echo $partnerId; ?>" required>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="partnerName" placeholder="Name" value="<?php echo $partnerName; ?>" required>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label>Email</label>
                <input type="text" readonly class="form-control" name="partnerEmail" value="<?php echo $partnerEmail; ?>" required>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label>Phone</label>
                <input type="number" class="form-control" name="partnerPhone" value="<?php echo $partnerPhone; ?>" required>
              </div>
            </div>
          </div>
       </div>
       <div class="card-header">
          Bank Details
       </div>
       <div class="card-body">
          <div class="row text-left">
            <div class="col-6">
              <div class="form-group">
                <label>Account Number</label>
                <input type="text" class="form-control" name="bankAccNo" placeholder="Acc No." value="<?php echo $accNo; ?>">
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label>IFSC Code</label>
                <input type="text" class="form-control" name="ifsc" placeholder="IFSC" value="<?php echo $ifsc; ?>">
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label>Bank Name</label>
                <input type="text" placeholder="Bank Name" class="form-control" name="bankName" value="<?php echo $bankName; ?>">
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <label>UPI ID</label>
                <input type="text" placeholder="UPI Id" class="form-control" name="upiId" value="<?php echo $upiId; ?>">
              </div>
            </div>
       </div> <hr>
       <input type="submit" class="btn btn-success" value="Submit" name="submitProfile">
     </form>
  </div>
	 
</div>
<?php else: ?>
  <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12 ml-auto mr-auto">
      <div class="alert">You are not allowed to access the page. Please <a href="signIn">Sign in</a> to see the page.</div>
    </div>
  </div>

<?php endif; ?>
